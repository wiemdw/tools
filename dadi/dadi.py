""" Date Difference Calculator """
import sys
import argparse
from datetime import datetime, timedelta

import pytz


sys.tracebacklimit = 0  # comment this line to get tracebacks on error

DEFAULT_FORMAT = "%Y-%m-%d"
DEFAULT_TIMEZONE = "UTC"

TIMEDELTA_UNIT_CONVERSION_MAP = {
    'seconds': lambda x: x,
    'minutes': lambda x: x / 60,
    'hours': lambda x: x / 3600,
    'days': lambda x: x / 86400,
    'weeks': lambda x: x / 604800,
}


def _parse_time(date: str, fmt: str) -> datetime:
    return datetime.strptime(date, fmt)


def _convert_timedelta_to_units(delta: timedelta, unit: str, precision: int = 1) -> float:
    delta_in_seconds = delta.total_seconds()
    if unit not in TIMEDELTA_UNIT_CONVERSION_MAP:
        raise ValueError(f"Unit {unit} is not supported.")
    return round(TIMEDELTA_UNIT_CONVERSION_MAP[unit](delta_in_seconds), precision)


def date_diff(date1: str, date2: str, fmt: str, tz: str, unit: str = None) -> str:
    """ Calculate date and time difference """
    # Parse strings to datetime objects
    date1 = _parse_time(date1, fmt)
    date2 = _parse_time(date2, fmt)

    # Convert timezones
    tzd = pytz.timezone(tz)
    date1 = tzd.localize(date1)
    date2 = tzd.localize(date2)

    # Calculate delta
    delta = date2 - date1

    if unit:
        delta_in_units = _convert_timedelta_to_units(delta, unit)
        return f"{delta_in_units} {unit}"
    return f"{str(delta)}"


parser = argparse.ArgumentParser(description='Calculate date and time difference.')
parser.add_argument('dates', metavar='N', type=str, nargs='+',
                    help='input dates')
parser.add_argument('--format', dest='format', action='store',
                    default=DEFAULT_FORMAT,
                    help='date string input format')
parser.add_argument('--timezone', dest='timezone', action='store',
                    default=DEFAULT_TIMEZONE,
                    help='timezone of the input dates')
parser.add_argument('--unit', dest='unit', action='store',
                    choices=list(TIMEDELTA_UNIT_CONVERSION_MAP.keys()),
                    help='unit for date difference output')

def main(args):
    return date_diff(date1=args.dates[0],
                     date2=args.dates[1],
                     fmt=args.format,
                     tz=args.timezone,
                     unit=args.unit)

if __name__ == '__main__':
    args = parser.parse_args()
    print(main(args))
