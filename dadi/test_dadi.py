import unittest
import pytz
from dadi import date_diff, DEFAULT_FORMAT, DEFAULT_TIMEZONE


class TestDadi(unittest.TestCase):
    """ Test Date Difference Calculator """
    def test_defaults(self):
        self.assertEqual(date_diff(date1="2020-01-01",
                                   date2="2020-01-02",
                                   fmt=DEFAULT_FORMAT,
                                   tz=DEFAULT_TIMEZONE),
                        "1 day, 0:00:00")

        self.assertEqual(date_diff(date1="2020-01-01",
                                   date2="2021-01-01",
                                   fmt=DEFAULT_FORMAT,
                                   tz=DEFAULT_TIMEZONE),
                        "366 days, 0:00:00")

        self.assertEqual(date_diff(date1="2021-01-01",
                                   date2="2020-01-01",
                                   fmt=DEFAULT_FORMAT,
                                   tz=DEFAULT_TIMEZONE),
                        "-366 days, 0:00:00")

    def test_timezones(self):
        self.assertEqual(date_diff(date1="2020-01-01",
                                   date2="2020-01-02",
                                   fmt=DEFAULT_FORMAT,
                                   tz="UTC"),
                        "1 day, 0:00:00")

        self.assertEqual(date_diff(date1="2020-01-01",
                                   date2="2020-01-02",
                                   fmt=DEFAULT_FORMAT,
                                   tz="Europe/Brussels"),
                        "1 day, 0:00:00")

    def test_timezones_with_dst(self):
        self.assertEqual(date_diff(date1="2020-10-24",
                                   date2="2020-10-26",
                                   fmt=DEFAULT_FORMAT,
                                   tz="Europe/Brussels"),
                        "2 days, 1:00:00")

    def test_timezones_with_dst_in_utc(self):
        self.assertEqual(date_diff(date1="2020-10-24",
                                   date2="2020-10-26",
                                   fmt=DEFAULT_FORMAT,
                                   tz="UTC"),
                        "2 days, 0:00:00")

    def test_units(self):
        self.assertEqual(date_diff(date1="2020-01-01",
                                   date2="2020-01-02",
                                   fmt=DEFAULT_FORMAT,
                                   unit="seconds",
                                   tz=DEFAULT_TIMEZONE),
                        "86400.0 seconds")

        self.assertEqual(date_diff(date1="2020-01-01",
                                   date2="2020-01-02",
                                   fmt=DEFAULT_FORMAT,
                                   unit="minutes",
                                   tz=DEFAULT_TIMEZONE),
                        "1440.0 minutes")

        self.assertEqual(date_diff(date1="2020-01-01",
                                   date2="2020-01-02",
                                   fmt=DEFAULT_FORMAT,
                                   unit="days",
                                   tz=DEFAULT_TIMEZONE),
                        "1.0 days")

        self.assertEqual(date_diff(date1="2020-01-01",
                                   date2="2020-01-02",
                                   fmt=DEFAULT_FORMAT,
                                   unit="weeks",
                                   tz=DEFAULT_TIMEZONE),
                        "0.1 weeks")

    def test_formats(self):
        self.assertEqual(date_diff(date1="2020 01 01",
                                   date2="2020 01 02",
                                   fmt="%Y %m %d",
                                   tz=DEFAULT_TIMEZONE),
                        "1 day, 0:00:00")

        self.assertEqual(date_diff(date1="2020 01 01 07:00",
                                   date2="2020 01 02 08:23",
                                   fmt="%Y %m %d %H:%M",
                                   tz=DEFAULT_TIMEZONE),
                        "1 day, 1:23:00")

        self.assertEqual(date_diff(date1="2020 01 01 07:00:12",
                                   date2="2020 01 02 08:23:57",
                                   fmt="%Y %m %d %H:%M:%S",
                                   tz=DEFAULT_TIMEZONE),
                        "1 day, 1:23:45")

    def test_exception_wrong_unit(self):
        with self.assertRaises(ValueError):
            date_diff(date1="2020-01-01",
                      date2="2020-01-02",
                      fmt=DEFAULT_FORMAT,
                      tz=DEFAULT_TIMEZONE,
                      unit="WRONG")

    def test_exception_wrong_timezone(self):
        with self.assertRaises(pytz.exceptions.UnknownTimeZoneError):
            date_diff(date1="2020-01-01",
                      date2="2020-01-02",
                      fmt=DEFAULT_FORMAT,
                      tz="WRONG")

    def test_exception_wrong_format(self):
        with self.assertRaises(ValueError):
            date_diff(date1="2020-01-01",
                      date2="2020-01-02",
                      fmt="WRONG",
                      tz=DEFAULT_TIMEZONE)
